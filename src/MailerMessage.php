<?php

namespace alexeevdv\yii\tidesoftware;

use yii\mail\BaseMessage;

class MailerMessage extends BaseMessage
{
    /**
     * @var string
     */
    private $_charset;

    /**
     * @var array|string
     */
    private $_from;

    /**
     * @var array|string
     */
    private $_to;

    /**
     * @var string|array
     */
    private $_replyTo;

    /**
     * @var string|array
     */
    private $_cc;

    /**
     * @var string|array
     */
    private $_bcc;

    /**
     * @var string
     */
    private $_subject;

    /**
     * @var string
     */
    private $_textBody;

    /**
     * @var string
     */
    private $_htmlBody;

    /**
     * @inheritDoc
     */
    public function getCharset()
    {
        return $this->_charset;
    }

    /**
     * @inheritDoc
     */
    public function setCharset($charset)
    {
        $this->_charset = $charset;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFrom()
    {
        return $this->_from;
    }

    /**
     * @inheritDoc
     */
    public function setFrom($from)
    {
        $this->_from = $from;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @return string
     */
    public function getToEmail()
    {
        $to = $this->getTo();

        if (is_array($to)) {
            $toEmails = array_keys($to);
            return reset($toEmails);
        }

        return $to;
    }

    /**
     * @inheritDoc
     */
    public function setTo($to)
    {
        $this->_to = $to;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getReplyTo()
    {
        return $this->_replyTo;
    }

    /**
     * @inheritDoc
     */
    public function setReplyTo($replyTo)
    {
        $this->_replyTo = $replyTo;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCc()
    {
        return $this->_cc;
    }

    /**
     * @inheritDoc
     */
    public function setCc($cc)
    {
        $this->_cc = $cc;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBcc()
    {
        return $this->_bcc;
    }

    /**
     * @inheritDoc
     */
    public function setBcc($bcc)
    {
        $this->_bcc = $bcc;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * @inheritDoc
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextBody()
    {
        return $this->_textBody;
    }

    /**
     * @inheritDoc
     */
    public function setTextBody($text)
    {
        $this->_textBody = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlBody()
    {
        return $this->_htmlBody;
    }

    /**
     * @inheritDoc
     */
    public function setHtmlBody($html)
    {
        $this->_htmlBody = $html;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function attach($fileName, array $options = [])
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function attachContent($content, array $options = [])
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function embed($fileName, array $options = [])
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function embedContent($content, array $options = [])
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function toString()
    {
        return $this->_htmlBody ?? $this->_textBody;
    }
}
