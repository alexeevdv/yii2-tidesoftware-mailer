<?php

namespace alexeevdv\yii\tidesoftware;

use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\helpers\Html;
use yii\mail\BaseMailer;

final class Mailer extends BaseMailer
{
    /**
     * @var string
     */
    public $password;

    /**
     * @var int
     */
    public $customerId;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $addressFromId;

    /**
     * @var string
     */
    public $wsdl = 'http://tideplatformgate.com/TideXMLGateStarter.asmx?WSDL';

    /**
     * @var string
     */
    public $messageClass = MailerMessage::class;

    /**
     * @var \SoapClient
     */
    public $soapClient;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if ($this->password === null) {
            throw new InvalidConfigException('`password` is required.');
        }

        if ($this->customerId === null) {
            throw new InvalidConfigException('`customerId` is required.');
        }

        if ($this->userId === null) {
            throw new InvalidConfigException('`userId` is required.');
        }

        if ($this->addressFromId === null) {
            throw new InvalidConfigException('`addressFromId` is required.');
        }

        $this->soapClient = $this->soapClient ?? new \SoapClient($this->wsdl);
        Instance::ensure($this->soapClient, \SoapClient::class);

        parent::init();
    }

    /**
     * @param MailerMessage $message
     * @return bool
     */
    protected function sendMessage($message): bool
    {
        $currentTime = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));

        $htmlBody = Html::encode($message->getHtmlBody());
        $textBody = Html::encode($message->getTextBody());

        $xml = <<<XML
<?xml version="1.0" encoding="utf-8" ?>
<TideEnvelope:Envelope
    xmlns:Tide="http://www.tideplatformgate.com/TideProtocol.xsd"
    xmlns:TideEnvelope="http://www.w3.org/2003/05/soap-envelope">
    <TideEnvelope:Header>
        <Tide:CustomersID value="{$this->customerId}" />
        <Tide:UsersID value="{$this->userId}" />
        <Tide:Password value="{$this->password}" />
        <Tide:Generate Date="{$currentTime->format('Y-m-d')}" Time="{$currentTime->format('H:i:s')}.000" />
        <Tide:Receipt Date="{$currentTime->format('Y-m-d')}" Time="{$currentTime->format('H:i:s')}.000" />
    </TideEnvelope:Header>
    <TideEnvelope:Body>
        <Tide:Process id="1" actualnode="1">
            <Tide:Node id="1" NodesID="48">
            <Tide:Datagroup id="1" />
                <Tide:Parameters>
                    <Tide:Parameter name="AddressesToAttributeClassID" value="1" />
                    <Tide:Parameter name="AddressesCCAttributeClassID" value="2" />
                    <Tide:Parameter name="AddressesBCCAttributeClassID" value="3" />
                    <Tide:Parameter name="ExternalIDAttributeClassID" value="4" />
                    <Tide:Parameter name="PriorityAttributeClassID" value="5" />
                    <Tide:Parameter name="IgnoreGIODOAttributeClassID" value="6" />
                    <Tide:Parameter name="SuspensionAttributeClassID" value="7" />
                    <Tide:Parameter name="GoogleUTMCodesIDAttributeClassID" value="8" />
                    <Tide:Parameter name="ConvertToInlineGraphicsAttributeClassID" value="9" />
                    <Tide:Parameter name="FooterAttributeClassID" value="10" />
                    <Tide:Parameter name="CustomersFooterIDAttributeClassID" value="11" />
                    <Tide:Parameter name="BulkAttributeClassID" value="12" />
                </Tide:Parameters>
            </Tide:Node>
        </Tide:Process>
        <Tide:Data>
            <Tide:Group id="1">
                <Tide:Items>
                    <Tide:Item id="1" name="Title">{$message->getSubject()}</Tide:Item>
                    <Tide:Item id="2" name="MailAddressesFromName" />
                    <Tide:Item id="3" name="MailAddressesFromID">{$this->addressFromId}</Tide:Item>
                    <Tide:Item id="4" name="MailCharsetTypesID">3</Tide:Item>
                    <Tide:Item id="5" name="CustomReplyToAddress" />
                    <Tide:Item id="6" name="HTMLBody">{$htmlBody}</Tide:Item>
                    <Tide:Item id="7" name="Body">{$textBody}</Tide:Item>
                    <Tide:Item id="8" name="MailAttributes">
                        <Tide:Attributes save="0">
                            <Tide:Attribute ExternalID="1" value="{$message->getToEmail()}" />
                            <Tide:Attribute ExternalID="6" value="1" />
                            <Tide:Attribute ExternalID="10" value="0" />
                        </Tide:Attributes>
                    </Tide:Item>
                </Tide:Items>
            </Tide:Group>
        </Tide:Data>
    </TideEnvelope:Body>
</TideEnvelope:Envelope>
XML;

        $response = $this->soapClient->PostXML(['xml' => $xml]);
        return strpos($response->PostXMLResult, 'OK ') === 0;
    }
}
