<?php

namespace tests\unit;

use alexeevdv\yii\tidesoftware\MailerMessage;
use Codeception\Test\Unit;

final class MailerMessageTest extends Unit
{
    public function testSetAndGetCharset(): void
    {
        $message = new MailerMessage();
        $result = $message->setCharset('utf-8');
        $this->assertSame('utf-8', $message->getCharset());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetFrom(): void
    {
        $message = new MailerMessage();
        $result = $message->setFrom('mail@example.org');
        $this->assertSame('mail@example.org', $message->getFrom());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetTo(): void
    {
        $message = new MailerMessage();
        $result = $message->setTo('mail@example.org');
        $this->assertSame('mail@example.org', $message->getTo());
        $this->assertSame($message, $result);
    }

    public function testGetToEmailFromString(): void
    {
        $message = new MailerMessage();
        $result = $message->setTo('mail@example.org');
        $this->assertSame('mail@example.org', $message->getToEmail());
        $this->assertSame($message, $result);
    }

    public function testGetToEmailFromArray(): void
    {
        $message = new MailerMessage();
        $result = $message->setTo(['mail@example.org' => 'Name']);
        $this->assertSame('mail@example.org', $message->getToEmail());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetReplyTo(): void
    {
        $message = new MailerMessage();
        $result = $message->setReplyTo('mail@example.org');
        $this->assertSame('mail@example.org', $message->getReplyTo());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetCc(): void
    {
        $message = new MailerMessage();
        $result = $message->setCc('mail@example.org');
        $this->assertSame('mail@example.org', $message->getCc());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetBcc(): void
    {
        $message = new MailerMessage();
        $result = $message->setBcc('mail@example.org');
        $this->assertSame('mail@example.org', $message->getBcc());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetSubject(): void
    {
        $message = new MailerMessage();
        $result = $message->setSubject('Test');
        $this->assertSame('Test', $message->getSubject());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetTextBody(): void
    {
        $message = new MailerMessage();
        $result = $message->setTextBody('Text');
        $this->assertSame('Text', $message->getTextBody());
        $this->assertSame($message, $result);
    }

    public function testSetAndGetHtmlBody(): void
    {
        $message = new MailerMessage();
        $result = $message->setHtmlBody('HTML');
        $this->assertSame('HTML', $message->getHtmlBody());
        $this->assertSame($message, $result);
    }

    public function testAttach(): void
    {
        $message = new MailerMessage();
        $result = $message->attach('filename');
        $this->assertSame($message, $result);
    }

    public function testAttachContent(): void
    {
        $message = new MailerMessage();
        $result = $message->attachContent('content');
        $this->assertSame($message, $result);
    }

    public function testEmbed(): void
    {
        $message = new MailerMessage();
        $result = $message->embed('filename');
        $this->assertSame('', $result);
    }

    public function testEmbedContent(): void
    {
        $message = new MailerMessage();
        $result = $message->embedContent('content');
        $this->assertSame('', $result);
    }

    public function testToStringWithHtmlBodyOnly()
    {
        $message = new MailerMessage();
        $message->setHtmlBody('HTML');
        $this->assertSame('HTML', $message->toString());
    }

    public function testToStringWithTextBodyOnly()
    {
        $message = new MailerMessage();
        $message->setTextBody('Text');
        $this->assertSame('Text', $message->toString());
    }

    public function testToStringWithBothHtmlAndTextBody()
    {
        $message = new MailerMessage();
        $message->setTextBody('Text');
        $message->setHtmlBody('HTML');
        $this->assertSame('HTML', $message->toString());
    }
}
