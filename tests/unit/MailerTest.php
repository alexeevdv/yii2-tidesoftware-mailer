<?php

namespace tests\unit;

use alexeevdv\yii\tidesoftware\Mailer;
use alexeevdv\yii\tidesoftware\MailerMessage;
use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use yii\base\InvalidConfigException;

final class MailerTest extends Unit
{
    public function testPasswordIsRequired(): void
    {
        $this->expectException(InvalidConfigException::class);
        new Mailer([
            'customerId' => 1,
            'userId' => 2,
            'addressFromId' => 3,
        ]);
    }

    public function testCustomerIdIsRequired(): void
    {
        $this->expectException(InvalidConfigException::class);
        new Mailer([
            'password' => 'password',
            'userId' => 2,
            'addressFromId' => 3,
        ]);
    }

    public function testUserIdIsRequired(): void
    {
        $this->expectException(InvalidConfigException::class);
        new Mailer([
            'password' => 'password',
            'customerId' => 1,
            'addressFromId' => 3,
        ]);
    }

    public function testAddressFromIdIsRequired(): void
    {
        $this->expectException(InvalidConfigException::class);
        new Mailer([
            'password' => 'password',
            'customerId' => 1,
            'userId' => 2,
        ]);
    }

    public function testSoapClientIsEnsured(): void
    {
        $this->expectException(InvalidConfigException::class);
        new Mailer([
            'password' => 'password',
            'customerId' => 1,
            'userId' => 2,
            'addressFromId' => 3,
            'soapClient' => 'invalid',
        ]);
    }

    public function testSuccessfulSendMessage(): void
    {
        $mailer = new Mailer([
            'password' => 'password',
            'customerId' => 1,
            'userId' => 2,
            'addressFromId' => 3,
            'soapClient' => $this->makeEmpty(\SoapClient::class, [
                '__call' => Expected::once(function () {
                    return (object)['PostXMLResult' => 'OK 123123'];
                }),
            ]),
        ]);

        $message = new MailerMessage([]);
        $this->assertTrue($mailer->send($message));
    }

    public function testFailedSendMessage(): void
    {
        $mailer = new Mailer([
            'password' => 'password',
            'customerId' => 1,
            'userId' => 2,
            'addressFromId' => 3,
            'soapClient' => $this->makeEmpty(\SoapClient::class, [
                '__call' => Expected::once(function () {
                    return (object)['PostXMLResult' => 'Anything except OK'];
                }),
            ]),
        ]);

        $message = new MailerMessage([]);
        $this->assertFalse($mailer->send($message));
    }
}
