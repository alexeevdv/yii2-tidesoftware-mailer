# Yii2 mailer for https://www.tidesoftware.pl/

## Installation

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run

```bash
$ composer require alexeevdv/yii2-tidesoftware-mailer "^0.1"
```

or add

```
"alexeevdv/yii2-tidesoftware-mailer": "^0.1"
```

to the ```require``` section of your `composer.json` file.

## Configuration

```php
[
    'components' => [  
        'mailer' => [
            'class' => alexeevdv\yii\tidesoftware\Mailer::class,
            'userId' => 1111,
            'password' => 'secret',
            'customerId' => 2222,
            'addressFromId' => 3333,
        ],
    ],
]
```
